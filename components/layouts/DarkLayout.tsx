import { FC, PropsWithChildren } from "react"


export const DarkLayout:FC<PropsWithChildren>= ({children}) => {
  return (
    <div style={{
        background:'rgba(0,0,0,0.3)',
        borderRadius:'5px',
        padding:'10px'
    }}>
        <h3>DarkLayout</h3>
        {children}</div>
  )
}
