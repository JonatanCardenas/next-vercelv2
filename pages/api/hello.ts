// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import { NextApiRequest, NextApiResponse } from "next";

//EL TIPADO DE DATOS NO DEBE LLEVAR ANY , SI LO USAMOS NO TIENE CASO UTILIZA ts:
type usuario = {
 name: string;
}
export default function handler(req:NextApiRequest, res:NextApiResponse<usuario>) {
  res.status(200).json({ name: 'John Doe'})
}
